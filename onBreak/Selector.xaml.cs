﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using Persistencia;

namespace onBreak
{
    /// <summary>
    /// Lógica de interacción para Selector.xaml
    /// </summary>
    public partial class Selector : MetroWindow
    {
        public Selector()
        {
            InitializeComponent();
        }

        private void TlClientes_Click(object sender, RoutedEventArgs e)
        {
            Clientes cl = new Clientes();
            cl.ShowDialog();
        }

        private void tlListaClientes_Click(object sender, RoutedEventArgs e)
        {
            ListaClientes lc = new ListaClientes();
            lc.ShowDialog();
        }

        private void tlContratos_Click(object sender, RoutedEventArgs e)
        {
            Contratos ct = new Contratos();
            ct.ShowDialog();
        }

        private void TlListaContratos_Click(object sender, RoutedEventArgs e)
        {
            ListaContratos tc = new ListaContratos();
            tc.ShowDialog();
        }
    }
}
