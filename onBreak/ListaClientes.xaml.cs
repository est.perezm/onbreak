﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using Persistencia;


namespace onBreak
{

    public partial class ListaClientes : MetroWindow
    {
        public ListaClientes()
        {
            InitializeComponent();
            CargarDataGrid();

        }

        private async void BtnBuscarListaClientes_Click(object sender, RoutedEventArgs e)
        {
            if(cmbCliente.SelectedIndex != -1)
            {
                CargarBusqueda();
            }

            else
            {
                await this.ShowMessageAsync("Lista de clientes", "Por favor, seleccione un criterio de busqueda");
            }
        }
        private void BtnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarFormulario();
            CargarDataGrid();
        }
        private void cmbClientes_Loaded(object sender, RoutedEventArgs e)
        {
            cmbCliente.Items.Add("Rut Cliente");
            cmbCliente.Items.Add("Código Empresa");
            cmbCliente.Items.Add("Código Actividad");
        }
        private void CargarDataGrid()
        {
            TContrato cto = new TContrato();
            TCliente cl = new TCliente();
            TModalidadServicio ms = new TModalidadServicio();
            TTipoEvento te = new TTipoEvento();
            TTipoEmpresa temp = new TTipoEmpresa();
            TActividadEmpresa act = new TActividadEmpresa();
            var fk = from c in cto.GetEntities()
                     join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                     join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                     join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                     join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                     join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa

                     select new
                     {
                         Rut = c.RutCliente,
                         Cod_Empresa = clte.IdTipoEmpresa,
                         Empresa = tem.Descripcion,
                         Cod_Actividad = ac.IdActividadEmpresa,
                         Actividad = ac.Descripcion,
                         RazonSocial = clte.RazonSocial,
                         Nombre = clte.NombreContacto,
                         Mail = clte.MailContacto,
                         Direccion = clte.Direccion,
                         Fono = clte.Telefono,

                     };
            dgClientes.ItemsSource = fk;
            dgClientes.Items.Refresh();
        }
        private async void CargarBusqueda()
        {
            try
            {
                if (cmbCliente.SelectedValue.ToString() == "Rut Cliente")
                {
                    TCliente tcliente = new TCliente();
                    Cliente cliente = tcliente.GetEntity(txtBuscarListaClientes.Text);

                    if (cliente != null)
                    {
                        TContrato cto = new TContrato();
                        TCliente cl = new TCliente();
                        TModalidadServicio ms = new TModalidadServicio();
                        TTipoEvento te = new TTipoEvento();
                        TTipoEmpresa temp = new TTipoEmpresa();
                        TActividadEmpresa act = new TActividadEmpresa();
                        var fk = from c in cto.GetEntities()
                                 join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                 join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                 join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                 join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                 join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                 where c.Cliente.RutCliente == txtBuscarListaClientes.Text

                                 select new
                                 {
                                     Rut = c.RutCliente,
                                     Cod_Empresa = clte.IdTipoEmpresa,
                                     Empresa = tem.Descripcion,
                                     Cod_Actividad = ac.IdActividadEmpresa,
                                     Actividad = ac.Descripcion,
                                     RazonSocial = clte.RazonSocial,
                                     Nombre = clte.NombreContacto,
                                     Mail = clte.MailContacto,
                                     Direccion = clte.Direccion,
                                     Fono = clte.Telefono,
                                 };
                        dgClientes.ItemsSource = fk;
                        dgClientes.Items.Refresh();
                    }
                    else
                    {
                        await this.ShowMessageAsync("Lista de clientes", "Registro no se encuentra en la base de datos");
                    }
                }
                else
                {
                    if (cmbCliente.SelectedValue.ToString() == "Código Empresa")
                    {
                        TTipoEmpresa tempr = new TTipoEmpresa();
                        TipoEmpresa emp = tempr.GetEntity(int.Parse(txtBuscarListaClientes.Text));

                        if (emp != null)
                        {
                            TContrato cto = new TContrato();
                            TCliente cl = new TCliente();
                            TModalidadServicio ms = new TModalidadServicio();
                            TTipoEvento te = new TTipoEvento();
                            TTipoEmpresa temp = new TTipoEmpresa();
                            TActividadEmpresa act = new TActividadEmpresa();
                            var fk = from c in cto.GetEntities()
                                     join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                     join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                     join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                     join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                     join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                     where c.Cliente.IdTipoEmpresa == int.Parse(txtBuscarListaClientes.Text)

                                     select new
                                     {
                                         Rut = c.RutCliente,
                                         Cod_Empresa = clte.IdTipoEmpresa,
                                         Empresa = tem.Descripcion,
                                         Cod_Actividad = ac.IdActividadEmpresa,
                                         Actividad = ac.Descripcion,
                                         RazonSocial = clte.RazonSocial,
                                         Nombre = clte.NombreContacto,
                                         Mail = clte.MailContacto,
                                         Direccion = clte.Direccion,
                                         Fono = clte.Telefono,
                                     };
                            dgClientes.ItemsSource = fk;
                            dgClientes.Items.Refresh();
                        }
                        else
                        {
                            await this.ShowMessageAsync("Lista de clientes", "Registro no se encuentra en la base de datos");
                        }
                    }
                    else
                    {
                        if (cmbCliente.SelectedValue.ToString() == "Código Actividad")
                        {
                            TActividadEmpresa tact = new TActividadEmpresa();
                            ActividadEmpresa actv = tact.GetEntity(int.Parse(txtBuscarListaClientes.Text));

                            if (actv != null)
                            {
                                TContrato cto = new TContrato();
                                TCliente cl = new TCliente();
                                TModalidadServicio ms = new TModalidadServicio();
                                TTipoEvento te = new TTipoEvento();
                                TTipoEmpresa temp = new TTipoEmpresa();
                                TActividadEmpresa act = new TActividadEmpresa();
                                var fk = from c in cto.GetEntities()
                                         join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                         join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                         join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                         join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                         join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                         where c.Cliente.IdActividadEmpresa == int.Parse(txtBuscarListaClientes.Text)

                                         select new
                                         {
                                             Rut = c.RutCliente,
                                             Cod_Empresa = clte.IdTipoEmpresa,
                                             Empresa = tem.Descripcion,
                                             Cod_Actividad = ac.IdActividadEmpresa,
                                             Actividad = ac.Descripcion,
                                             RazonSocial = clte.RazonSocial,
                                             Nombre = clte.NombreContacto,
                                             Mail = clte.MailContacto,
                                             Direccion = clte.Direccion,
                                             Fono = clte.Telefono,
                                         };
                                dgClientes.ItemsSource = fk;
                                dgClientes.Items.Refresh();
                            }
                            else
                            {
                                await this.ShowMessageAsync("Lista de clientes", "Registro no se encuentra en la base de datos");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                await this.ShowMessageAsync("Lista de clientes", "La busqueda no corresponde");
            }
        }
        private void LimpiarFormulario()
        {
            cmbCliente.SelectedIndex = -1;
            txtBuscarListaClientes.Text = String.Empty;
        }
    }
}
