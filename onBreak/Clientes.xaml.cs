﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using Persistencia;
using System.Text.RegularExpressions;

namespace onBreak
{
    /// <summary>
    /// Lógica de interacción para Clientes.xaml
    /// </summary>
    public partial class Clientes : MetroWindow
    {
        public Clientes()
        {
            InitializeComponent();
            CargarActividadEmpresa();
            CargarTipoEmpresa();
        }

        private async void BtnAgregarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if(txtRut.Text == "" || txtRazonSocial.Text == "" || txtNombre.Text == "" || cmbActividadEmpresa.SelectedIndex == -1 || txtEmail.Text == "" ||txtDireccion.Text == "" || txtTelefono.Text == ""
                ||cmbTipoEmpresa.SelectedIndex == -1)
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Hay campos sin llenar, favor verificar");
            }

            else
            {
                RegistrarCliente();
            }
        }

        private async void BtnModificarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (txtBuscarPorRut.Text != "")
            {
                ModificarCliente();
            }

            else
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Por favor, realice primero una busqueda");
            }
        }

        private async void btnBuscarRut_Click(object sender, RoutedEventArgs e)
        {
            if (txtBuscarPorRut.Text != "")
            {
                BuscarClienteRut();
            }

            else
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Ingrese un valor para buscar");
            }

        }

        private async void BtnEliminarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if(txtRut.Text != "")
            {
                EliminarCliente(); // ver si lo vamos a dar de baja, si no lo vamos a eliminar, en que campo colocamos el estado?
            }

            else
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Por favor, realice primero una busqueda");
            }
                
            
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarFormulario();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtNombre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            foreach (char c in e.Text)
            {
                if (!char.IsLetter(c))
                {
                    e.Handled = true;
                    break;
                }
            }
    }

        private async void TxtRut_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtRut.Text != "")
            {
                if (validarRut(txtRut.Text) == false)
                {
                    await this.ShowMessageAsync("Administrador de usuarios", "Formato de RUT invalido, Ingrese formato correcto");
                    txtRut.Clear();
                    return;
                }
            }

        }

        private async void TxtEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if(txtEmail.Text != "")
            {
                Regex reg = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                if (!reg.IsMatch(txtEmail.Text))
                {
                    await this.ShowMessageAsync("Administrador de usuarios", "Formato de correo invalido, Ingrese formato correcto");
                    txtEmail.Text = "";
                }
            }

        }

        // INICIO METODOS PANTALLA

        private async void RegistrarCliente()
        {
            try
            {
                Cliente c = new Cliente
                {

                    RutCliente = txtRut.Text,
                    RazonSocial = txtRazonSocial.Text,
                    NombreContacto = txtNombre.Text,
                    MailContacto = txtEmail.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    IdActividadEmpresa = (int)cmbActividadEmpresa.SelectedValue,
                    IdTipoEmpresa = (int)cmbTipoEmpresa.SelectedValue
                };


                TCliente cl = new TCliente();
                cl.AddEntity(c);
                await this.ShowMessageAsync("Administrador de usuarios", "Cliente registrado correctamente");
                LimpiarFormulario();
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Administrador de usuarios", "No es posible agregar cliente, Mensaje:" + ex.Message);
            }
        }

        private async void ModificarCliente()
        {
            try
            {
                Cliente c = new Cliente
                {
                    RutCliente = txtRut.Text,
                    RazonSocial = txtRazonSocial.Text,
                    NombreContacto = txtNombre.Text,
                    MailContacto = txtEmail.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    IdActividadEmpresa = (int)cmbActividadEmpresa.SelectedValue,
                    IdTipoEmpresa = (int)cmbTipoEmpresa.SelectedValue

                };

                TCliente cl = new TCliente();
                cl.UpdateEntity(c);
                await this.ShowMessageAsync("Administrador de usuarios", "Cliente actualizado correctamente");
                LimpiarFormulario();
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Administrador de usuarios", "No es posible modificar registro debido a que no existe, Mensaje: "+ex.Message);
            }
        }

        private async void EliminarCliente()
        {
            try
            {
                string RutCliente = txtRut.Text;
                TCliente cliente = new TCliente();
                TContrato contrato = new TContrato();

                var FiltroRut = from c in cliente.GetEntities() 
                                      join co in contrato.GetEntities()
                                      on c.RutCliente equals co.RutCliente
                                      where c.RutCliente == txtRut.Text

                                      select new
                                      {
                                          contrato = c.Contrato
                                      };
                var Resultado = FiltroRut.Count();

                if (Resultado != 0)
                {
                  await this.ShowMessageAsync("Administrador de usuarios", "Registro no puede ser eliminado debido a que presenta contrato asociado");
                    return;
                }

                else
                {
                    var mySettings = new MetroDialogSettings()
                    {
                        AffirmativeButtonText = "Si",
                        NegativeButtonText = "No",
                        ColorScheme = MetroDialogOptions.ColorScheme
                    };

                    MahApps.Metro.Controls.Dialogs.MessageDialogResult result = await this.ShowMessageAsync("Eliminar Usuario", "¿Esta seguro de realizar esta accion?", MessageDialogStyle.AffirmativeAndNegative, mySettings);

                    if (result == MessageDialogResult.Affirmative)
                    {
                        new TCliente().DeleteEntity(RutCliente);
                        await this.ShowMessageAsync("Administrador de usuarios", "Usuario eliminado con exito ");
                        LimpiarFormulario();
                    }

                }
            }
            catch (Exception ex)
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Error de aplicacion, mensaje: "+ex.Message);
            }
        }



        private void CargarActividadEmpresa()
        {
            TTipoEmpresa te = new TTipoEmpresa();
            cmbTipoEmpresa.ItemsSource = te.GetEntities();
            cmbTipoEmpresa.SelectedValuePath = "IdTipoEmpresa";
            cmbTipoEmpresa.DisplayMemberPath = "Descripcion";
        }

        private void CargarTipoEmpresa()
        {
            TActividadEmpresa ae = new TActividadEmpresa();
            cmbActividadEmpresa.ItemsSource = ae.GetEntities();
            cmbActividadEmpresa.SelectedValuePath = "IdActividadEmpresa";
            cmbActividadEmpresa.DisplayMemberPath = "Descripcion";
        }



        private async void BuscarClienteRut()
        {
            try
            {
                string RutCliente = txtBuscarPorRut.Text;
                TCliente cl = new TCliente();
                Cliente c = cl.GetEntity(RutCliente);

                if (c == cl.GetEntity(RutCliente))
                {
                    txtRut.Text = c.RutCliente;
                    txtRazonSocial.Text = c.RazonSocial;
                    txtNombre.Text = c.NombreContacto;
                    txtEmail.Text = c.MailContacto;
                    txtDireccion.Text = c.Direccion;
                    txtTelefono.Text = c.Telefono;
                    cmbActividadEmpresa.SelectedValue = c.IdActividadEmpresa;
                    cmbTipoEmpresa.SelectedValue = c.IdTipoEmpresa;

                }
            }
            catch (Exception)
            {
                await this.ShowMessageAsync("Administrador de usuarios", "Cliente no se encuentra en la base de datos");
            }
        }

        private void LimpiarFormulario()
        {
            txtRut.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            cmbActividadEmpresa.SelectedIndex = -1;
            cmbTipoEmpresa.SelectedIndex = -1;
            txtRut.Focus();
            CargarActividadEmpresa();
            CargarTipoEmpresa();
            txtBuscarPorRut.Text = string.Empty;

        }

        private void SoloAceptoNumeros(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        public bool validarRut(string rut)
        {

            bool validacion = false;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }
    }
}