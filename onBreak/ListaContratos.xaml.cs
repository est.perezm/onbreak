﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using Persistencia;

namespace onBreak
{

    public partial class ListaContratos : MetroWindow
    {
        public ListaContratos()
        {
            InitializeComponent();
            CargarDataGrid();
        }

        private async void btnBuscarContratos_Click(object sender, RoutedEventArgs e)
        {
            if(cmbContrato.SelectedIndex != -1)
            {
                CargarBusqueda();
            }

            else
            {
                await this.ShowMessageAsync("Lista de contratos", "Por favor, seleccione un criterio de busqueda");
            }
            
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarFormulario();
            CargarDataGrid();
        }
        private void CargarDataGrid()
        {
            TContrato cto = new TContrato();
            TCliente cl = new TCliente();
            TModalidadServicio ms = new TModalidadServicio();
            TTipoEvento te = new TTipoEvento();
            TTipoEmpresa temp = new TTipoEmpresa();
            TActividadEmpresa act = new TActividadEmpresa();
            var fk = from c in cto.GetEntities()
                     join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                     join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                     join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                     join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                     join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa

                     select new
                     {
                         Rut = c.RutCliente,
                         Contrato = c.Numero,
                         Cod_Empresa = clte.IdTipoEmpresa,
                         Empresa = tem.Descripcion,
                         Cod_Actividad = ac.IdActividadEmpresa,
                         Actividad = ac.Descripcion,
                         Creacion = c.Creacion,
                         Termino = c.Termino,
                         ID_Servicio = c.IdModalidad,
                         Servicio = ser.Nombre,
                         ID_Evento = c.IdTipoEvento,
                         Evento = tev.Descripcion,
                         F_Inicio = c.FechaHoraInicio,
                         F_Termino = c.FechaHoraTermino,
                         Asistente = c.Asistentes,
                         Adicional = c.PersonalAdicional,
                         Realizado = c.Realizado,
                         Valor = c.ValorTotalContrato,
                         Obs = c.Observaciones

                     };
            dgContratos.ItemsSource = fk;
            dgContratos.Items.Refresh();
        }

        private async void CargarBusqueda()
        {
            try
            {
                if (cmbContrato.SelectedValue.ToString() == "Rut Cliente")
                {
                    TCliente tcliente = new TCliente();
                    Cliente cliente = tcliente.GetEntity(txtBuscarContrato.Text);

                    if (cliente != null)
                    {
                        TContrato cto = new TContrato();
                        TCliente cl = new TCliente();
                        TModalidadServicio ms = new TModalidadServicio();
                        TTipoEvento te = new TTipoEvento();
                        TTipoEmpresa temp = new TTipoEmpresa();
                        TActividadEmpresa act = new TActividadEmpresa();
                        var fk = from c in cto.GetEntities()
                                 join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                 join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                 join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                 join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                 join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                 where c.Cliente.RutCliente == txtBuscarContrato.Text

                                 select new
                                 {
                                     Rut = c.RutCliente,
                                     Contrato = c.Numero,
                                     Cod_Empresa = clte.IdTipoEmpresa,
                                     Empresa = tem.Descripcion,
                                     Cod_Actividad = ac.IdActividadEmpresa,
                                     Actividad = ac.Descripcion,
                                     Creacion = c.Creacion,
                                     Termino = c.Termino,
                                     ID_Servicio = c.IdModalidad,
                                     Servicio = ser.Nombre,
                                     ID_Evento = c.IdTipoEvento,
                                     Evento = tev.Descripcion,
                                     F_Inicio = c.FechaHoraInicio,
                                     F_Termino = c.FechaHoraTermino,
                                     Asistente = c.Asistentes,
                                     Adicional = c.PersonalAdicional,
                                     Realizado = c.Realizado,
                                     Valor = c.ValorTotalContrato,
                                     Obs = c.Observaciones
                                 };
                        dgContratos.ItemsSource = fk;
                        dgContratos.Items.Refresh();
                    }
                    else
                    {
                        await this.ShowMessageAsync("Lista de contratos", "Registro no se encuentra en la base de datos");
                    }
                }
                else
                {
                    if (cmbContrato.SelectedValue.ToString() == "Núm.Contrato")
                    {
                        TContrato cont = new TContrato();
                        Contrato contrato = cont.GetEntity(txtBuscarContrato.Text);

                        if (contrato != null)
                        {
                            TContrato cto = new TContrato();
                            TCliente cl = new TCliente();
                            TModalidadServicio ms = new TModalidadServicio();
                            TTipoEvento te = new TTipoEvento();
                            TTipoEmpresa temp = new TTipoEmpresa();
                            TActividadEmpresa act = new TActividadEmpresa();
                            var fk = from c in cto.GetEntities()
                                     join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                     join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                     join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                     join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                     join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                     where c.Numero == txtBuscarContrato.Text

                                     select new
                                     {
                                         Rut = c.RutCliente,
                                         Contrato = c.Numero,
                                         Cod_Empresa = clte.IdTipoEmpresa,
                                         Empresa = tem.Descripcion,
                                         Cod_Actividad = ac.IdActividadEmpresa,
                                         Actividad = ac.Descripcion,
                                         Creacion = c.Creacion,
                                         Termino = c.Termino,
                                         ID_Servicio = c.IdModalidad,
                                         Servicio = ser.Nombre,
                                         ID_Evento = c.IdTipoEvento,
                                         Evento = tev.Descripcion,
                                         F_Inicio = c.FechaHoraInicio,
                                         F_Termino = c.FechaHoraTermino,
                                         Asistente = c.Asistentes,
                                         Adicional = c.PersonalAdicional,
                                         Realizado = c.Realizado,
                                         Valor = c.ValorTotalContrato,
                                         Obs = c.Observaciones
                                     };
                            dgContratos.ItemsSource = fk;
                            dgContratos.Items.Refresh();
                        }
                        else
                        {
                            await this.ShowMessageAsync("Lista de contratos", "Registro no se encuentra en la base de datos");
                        }
                    }
                    else
                    {
                        if (cmbContrato.SelectedValue.ToString() == "Código Empresa")
                        {
                            TTipoEmpresa tempr = new TTipoEmpresa();
                            TipoEmpresa emp = tempr.GetEntity(int.Parse(txtBuscarContrato.Text));

                            if (emp != null)
                            {
                                TContrato cto = new TContrato();
                                TCliente cl = new TCliente();
                                TModalidadServicio ms = new TModalidadServicio();
                                TTipoEvento te = new TTipoEvento();
                                TTipoEmpresa temp = new TTipoEmpresa();
                                TActividadEmpresa act = new TActividadEmpresa();
                                var fk = from c in cto.GetEntities()
                                         join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                         join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                         join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                         join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                         join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                         where c.Cliente.IdTipoEmpresa == int.Parse(txtBuscarContrato.Text)

                                         select new
                                         {
                                             Rut = c.RutCliente,
                                             Contrato = c.Numero,
                                             Cod_Empresa = clte.IdTipoEmpresa,
                                             Empresa = tem.Descripcion,
                                             Cod_Actividad = ac.IdActividadEmpresa,
                                             Actividad = ac.Descripcion,
                                             Creacion = c.Creacion,
                                             Termino = c.Termino,
                                             ID_Servicio = c.IdModalidad,
                                             Servicio = ser.Nombre,
                                             ID_Evento = c.IdTipoEvento,
                                             Evento = tev.Descripcion,
                                             F_Inicio = c.FechaHoraInicio,
                                             F_Termino = c.FechaHoraTermino,
                                             Asistente = c.Asistentes,
                                             Adicional = c.PersonalAdicional,
                                             Realizado = c.Realizado,
                                             Valor = c.ValorTotalContrato,
                                             Obs = c.Observaciones
                                         };
                                dgContratos.ItemsSource = fk;
                                dgContratos.Items.Refresh();
                            }
                            else
                            {
                                await this.ShowMessageAsync("Lista de contratos", "Registro no se encuentra en la base de datos");
                            }
                        }
                        else
                        {
                            if (cmbContrato.SelectedValue.ToString() == "Código Actividad")
                            {
                                TContrato cto = new TContrato();
                                TCliente cl = new TCliente();
                                TModalidadServicio ms = new TModalidadServicio();
                                TTipoEvento te = new TTipoEvento();
                                TTipoEmpresa temp = new TTipoEmpresa();
                                TActividadEmpresa act = new TActividadEmpresa();
                                var fk = from c in cto.GetEntities()
                                         join clte in cl.GetEntities() on c.RutCliente equals clte.RutCliente
                                         join ser in ms.GetEntities() on c.IdModalidad equals ser.IdModalidad
                                         join tev in te.GetEntities() on c.IdTipoEvento equals tev.IdTipoEvento
                                         join tem in temp.GetEntities() on clte.IdTipoEmpresa equals tem.IdTipoEmpresa
                                         join ac in act.GetEntities() on clte.IdActividadEmpresa equals ac.IdActividadEmpresa
                                         where c.Cliente.IdActividadEmpresa == int.Parse(txtBuscarContrato.Text)

                                         select new
                                         {
                                             Rut = c.RutCliente,
                                             Contrato = c.Numero,
                                             Cod_Empresa = clte.IdTipoEmpresa,
                                             Empresa = tem.Descripcion,
                                             Cod_Actividad = ac.IdActividadEmpresa,
                                             Actividad = ac.Descripcion,
                                             Creacion = c.Creacion,
                                             Termino = c.Termino,
                                             ID_Servicio = c.IdModalidad,
                                             Servicio = ser.Nombre,
                                             ID_Evento = c.IdTipoEvento,
                                             Evento = tev.Descripcion,
                                             F_Inicio = c.FechaHoraInicio,
                                             F_Termino = c.FechaHoraTermino,
                                             Asistente = c.Asistentes,
                                             Adicional = c.PersonalAdicional,
                                             Realizado = c.Realizado,
                                             Valor = c.ValorTotalContrato,
                                             Obs = c.Observaciones
                                         };
                                dgContratos.ItemsSource = fk;
                                dgContratos.Items.Refresh();
                            }
                            else
                            {
                                await this.ShowMessageAsync("Lista de contratos", "Registro no se encuentra en la base de datos");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                await this.ShowMessageAsync("Lista de contratos", "La busqueda no corresponde");
            }
        }
        private void LimpiarFormulario()
        {
            cmbContrato.SelectedIndex = -1;
            txtBuscarContrato.Text = String.Empty;
        }
        private void CmbContrato_Loaded(object sender, RoutedEventArgs e)
        {
            cmbContrato.Items.Add("Rut Cliente");
            cmbContrato.Items.Add("Núm.Contrato");
            cmbContrato.Items.Add("Código Empresa");
            cmbContrato.Items.Add("Código Actividad");

        }

    }
}