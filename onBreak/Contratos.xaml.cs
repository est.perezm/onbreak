﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using Persistencia;
using System.Globalization;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace onBreak
{
    /// <summary>
    /// Lógica de interacción para Clientes.xaml
    /// </summary>
    /// LISTA DE METODOS AL FINAL DEL CODIGO
    /// 

    public partial class Contratos : MetroWindow

    {
        DateTime fechaActual = DateTime.Now;
        //Variables Calculo Valor Total
        int ValorTotal;
        int ValorBase = 0;
        int RecargoAsistente = 0;
        int RecargoPersonal = 0;
        int ValorUf = 27784;
        //Variable Temp
        string tempmodalidadevento = "";

        public Contratos()
        {
            InitializeComponent();
            CargarTipoEvento();
            CargarActividadEmpresa();
            CargarTipoEmpresa();
            CalculoValor();
        }

        private void CmbTipoEvento_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarModalidadSerivicio();
        }

        private void BtnTerminarContrato_Click(object sender, RoutedEventArgs e)
        {
            TerminarContrato();
        }

        private void btnSiguiente_click(object sender, RoutedEventArgs e)
        {
            tcContratos.SelectedIndex = 1;
        }

        private void btnAtras_click(object sender, RoutedEventArgs e)
        {
            tcContratos.SelectedIndex = 0;
        }

        private void BtnBuscarPorRut_Click(object sender, RoutedEventArgs e)
        {
            BuscarPorRut();
        }

        private void BtnAgregarContrato_Click(object sender, RoutedEventArgs e)
        {
            AgregarContrato();
        }

        private void BtnBuscarPorContrato_Click(object sender, RoutedEventArgs e)
        {
            BuscarPorContrato();
        }

        private void BtnBuscarContrato_Click(object sender, RoutedEventArgs e)
        {
            buscarContrato();
        }

        private void Btnlimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarContrato();
        }

        private void BtnLimpiarContacto_Click(object sender, RoutedEventArgs e)
        {
            LimpiarUsuario();
        }

        private void BtnCerrar2_Click(object sender, RoutedEventArgs e)
        {
            CerrarVentana();
        }

        private void BtnCerrar1_Click(object sender, RoutedEventArgs e)
        {
            CerrarVentana();
        }

        private void TxtAsistentes_LostFocus(object sender, RoutedEventArgs e)
        {
            if(txtAsistentes.Text != "")
            {
                double CantidadAsistentes;
                CantidadAsistentes = double.Parse(txtAsistentes.Text);

                if (CantidadAsistentes >= 1 & CantidadAsistentes <= 20)
                {
                    RecargoAsistente = 3 * ValorUf;
                }

                if (CantidadAsistentes >= 21 & CantidadAsistentes <= 50)
                {
                    RecargoAsistente = 5 * ValorUf;
                }

                if (CantidadAsistentes >= 51)
                {
                    double PorAdicional = 20;
                    RecargoAsistente = (2 * (int)Math.Ceiling(CantidadAsistentes / PorAdicional)) * ValorUf;
                }

                CalculoValor();
            }

        }

        private void TxtPersonalAdicional_LostFocus(object sender, RoutedEventArgs e)
        {
            if(txtPersonalAdicional.Text != "")
            {
                double CantidadPersonal;
                CantidadPersonal = double.Parse(txtPersonalAdicional.Text);
                {

                    if (CantidadPersonal == 2)
                    {
                        RecargoPersonal = 2 * ValorUf;
                    }

                    if (CantidadPersonal == 3)
                    {
                        RecargoPersonal = 3 * ValorUf;
                    }

                    if (CantidadPersonal == 4)
                    {
                        RecargoPersonal = (int)Math.Round(3.5 * ValorUf);
                    }

                    if (CantidadPersonal > 4)
                    {
                        double AdicionalPersonal;
                        AdicionalPersonal = CantidadPersonal - 4;
                        RecargoPersonal = (int)Math.Round((3.5 + (0.5 * AdicionalPersonal)) * ValorUf);
                    }

                    CalculoValor();
                }
            }

        }

        private void CmbModalidadServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cmbModalidadServicio.SelectedIndex != -1)
            {
                int Vbase;
                int Pbase;
                TModalidadServicio tmodalidadServicio = new TModalidadServicio();
                if (cmbModalidadServicio.SelectedIndex == -1)
                {
                    ValorBase = 0;
                    CalculoValor();
                }
                else
                {
                    if (tempmodalidadevento != "")
                    {
                        ModalidadServicio modalidadServicio = tmodalidadServicio.GetEntity(tempmodalidadevento);
                        Vbase = Convert.ToInt32(modalidadServicio.ValorBase);
                        Pbase = Convert.ToInt32(modalidadServicio.PersonalBase);
                        ValorBase = (Vbase + Pbase) * ValorUf;
                        CalculoValor();
                        tempmodalidadevento = "";
                    }
                    else
                    {

                        ModalidadServicio modalidadServicio = tmodalidadServicio.GetEntity(cmbModalidadServicio.SelectedValue);
                        Vbase = Convert.ToInt32(modalidadServicio.ValorBase);
                        Pbase = Convert.ToInt32(modalidadServicio.PersonalBase);
                        ValorBase = (Vbase + Pbase) * ValorUf;
                        CalculoValor();
                    }
                }
            }

        }

        private void TxtBuscarPorRut_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnBuscarPorRut_Click(sender, e);
            }
        }

        private void TxtBuscarPorContrato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnBuscarPorContrato_Click(sender, e);
            }
        }

        private void TxtBuscarContrato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnBuscarContrato_Click(sender, e);
            }
        }

        // LISTA DE METODOS APLICADOS

        private async void AgregarContrato()
        {
            try
            {
                if (txtRut.Text == "" || txtRazonSocial.Text == "" || txtNombre.Text == "" || txtEmail.Text == "" || txtDireccion.Text == "" || txtTelefono.Text == "" ||
                    cmbActividadEmpresa.SelectedIndex == -1 || cmbTipoEmpresa.SelectedIndex == -1)
                {
                    await this.ShowMessageAsync("Administrador de contratos", "Faltan datos de cliente, favor revisar campos vacios");
                    return;
                }

                if (dpInicioContrato.SelectedDate == null || dpFinContrato.SelectedDate == null || FechaInicio.SelectedDate == null || FechaTermino.SelectedDate == null ||
                    cmbTipoEvento.SelectedIndex == -1 || cmbModalidadServicio.SelectedIndex == -1 || txtAsistentes.Text == "" || txtValorTotal.Text == "" || txtPersonalAdicional.Text == "" ||
                    txtObservaciones.Text == "")
                {
                    await this.ShowMessageAsync("Administrador de contratos", "Faltan datos del contrato, favor revisar campos vacios");
                    return;
                }

                else
                {
                    if (dpFinContrato.SelectedDate.Value.CompareTo(dpInicioContrato.SelectedDate.Value) < 1)
                    {
                        await this.ShowMessageAsync("Administrador de contratos", "La fecha de finalizacion del contrato debe ser mayor a la del inicio de este");
                        dpFinContrato.SelectedDate = null;
                        return;
                    }
                    if (dpInicioContrato.SelectedDate.Value.CompareTo(FechaInicio.SelectedDate.Value) > 0 || dpFinContrato.SelectedDate.Value.CompareTo(FechaTermino.SelectedDate.Value) < 0)
                    {
                        await this.ShowMessageAsync("Administrador de contratos", "La fecha de inicio / termino de contrato debe ser mayor a la fecha de inicio de contrato y menor a la de termino");
                        FechaInicio.SelectedDate = null;
                        FechaTermino.SelectedDate = null;
                        return;
                    }

                    if  (FechaInicio.SelectedDate.Value.CompareTo(FechaTermino.SelectedDate.Value) == 0 && TPHoraTerminoEvento.SelectedTime.Value.CompareTo(TPHoraInicioEvento.SelectedTime.Value) < 1)
                    {
                        await this.ShowMessageAsync("Administrador de contratos", "Si el evento es de 1 solo día, La hora de termino debe ser mayor a la de inicio");
                        TPHoraTerminoEvento.SelectedTime = null;
                        return;
                    }

                    else
                    {
                        DateTime fechahorainicio = Convert.ToDateTime(FechaInicio.SelectedDate + TPHoraInicioEvento.SelectedTime);
                        DateTime fechahoratermino = Convert.ToDateTime(FechaTermino.SelectedDate + TPHoraTerminoEvento.SelectedTime);

                        TContrato tcontrato = new TContrato();
                        Contrato contrato = new Contrato
                        {
                            Numero = fechaActual.ToString("yyyyMMddHHmm"),
                            Creacion = dpInicioContrato.SelectedDate.Value,
                            Termino = dpFinContrato.SelectedDate.Value,
                            RutCliente = txtRut.Text,
                            IdTipoEvento = (int)cmbTipoEvento.SelectedValue,
                            IdModalidad = cmbModalidadServicio.SelectedValue.ToString(),
                            FechaHoraInicio = fechahorainicio,
                            FechaHoraTermino = fechahoratermino,
                            Asistentes = int.Parse(txtAsistentes.Text),
                            PersonalAdicional = int.Parse(txtPersonalAdicional.Text),
                            Realizado = chbRealizado.IsChecked == true, // //REVISAR no es combobox
                            ValorTotalContrato = 10000, //REVISAR donde esta el valor base
                            Observaciones = txtObservaciones.Text
                        };

                        tcontrato.AddEntity(contrato);
                        await this.ShowMessageAsync("Administrador de contratos", "Contrato registrado correctamente");
                        LimpiarUsuario();
                        LimpiarContrato();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }

        private async void TerminarContrato()
        {
            try
            {
                if (txtBuscarContrato.Text == ""  && txtAsistentes.Text == "")
                {
                    await this.ShowMessageAsync("Administrador de contratos", "No hay contrato en pantalla");
                    return;
                }

                if (chbRealizado.IsChecked == true)
                {
                    await this.ShowMessageAsync("Administrador de contratos", "Contrato ya terminado en sistema");
                    return;
                }

                else
                {
                    var mySettings = new MetroDialogSettings()
                    {
                        AffirmativeButtonText = "Si",
                        NegativeButtonText = "No",
                        ColorScheme = MetroDialogOptions.ColorScheme
                    };

                    MahApps.Metro.Controls.Dialogs.MessageDialogResult result = await this.ShowMessageAsync("Terminar contrato", "¿Esta seguro de realizar esta accion?", MessageDialogStyle.AffirmativeAndNegative, mySettings);

                    if (result == MessageDialogResult.Affirmative)
                    {
                        TContrato tcontrato = new TContrato();
                        Contrato contrato = new Contrato
                        {
                            Numero = txtBuscarContrato.Text,
                            Realizado = true,
                            Termino = DateTime.Now
                        };

                        tcontrato.UpdateEstadoContrato(contrato);
                        await this.ShowMessageAsync("Administrador de contratos", "Contrato Terminado correctamente");
                        buscarContrato();

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }

        private async void buscarContrato()
        {
            try
            {
                TContrato tcontrato = new TContrato();
                Contrato contrato = tcontrato.GetEntity(txtBuscarContrato.Text);

                if (contrato != null)
                {
                    //Llena campos de contrato
                    dpInicioContrato.SelectedDate = contrato.Creacion;
                    dpFinContrato.SelectedDate = contrato.Termino;
                    FechaInicio.SelectedDate = contrato.FechaHoraInicio;
                    FechaTermino.SelectedDate = contrato.FechaHoraTermino;

                    TimeSpan HoraInicio = new TimeSpan(contrato.FechaHoraInicio.Hour, contrato.FechaHoraInicio.Minute, contrato.FechaHoraInicio.Second);
                    TimeSpan HoraTermino = new TimeSpan(contrato.FechaHoraTermino.Hour, contrato.FechaHoraTermino.Minute, contrato.FechaHoraTermino.Second);

                    TPHoraInicioEvento.SelectedTime = HoraInicio;
                    TPHoraTerminoEvento.SelectedTime = HoraTermino;
                    cmbTipoEvento.SelectedValue = contrato.IdTipoEvento;
                    tempmodalidadevento = contrato.IdModalidad;
                    CargarModalidadSerivicio();
                    cmbModalidadServicio.SelectedValue = contrato.IdModalidad;
                    txtAsistentes.Text = contrato.Asistentes.ToString();
                    txtPersonalAdicional.Text = contrato.PersonalAdicional.ToString();
                    chbRealizado.IsChecked = contrato.Realizado;
                    txtObservaciones.Text = contrato.Observaciones;
                    txtValorTotal.Text = contrato.ValorTotalContrato.ToString();

                    //Llena campos de cliente
                    txtRut.Text = contrato.Cliente.RutCliente;
                    txtRazonSocial.Text = contrato.Cliente.RazonSocial;
                    txtNombre.Text = contrato.Cliente.NombreContacto;
                    txtEmail.Text = contrato.Cliente.MailContacto;
                    txtDireccion.Text = contrato.Cliente.Direccion;
                    txtTelefono.Text = contrato.Cliente.Telefono;
                    cmbActividadEmpresa.SelectedValue = contrato.Cliente.IdActividadEmpresa;
                    cmbTipoEmpresa.SelectedValue = contrato.Cliente.IdTipoEmpresa;
                }

                else
                {
                    await this.ShowMessageAsync("Administrador de contratos", "Contrato no encontrado");
                }

            }

            catch (Exception ex)
            {
                await this.ShowMessageAsync("Error de aplicacion", ex.Message);
                throw;
            }

        }

        private void LimpiarContrato()
        {
            dpInicioContrato.SelectedDate = null;
            dpFinContrato.SelectedDate = null;
            FechaInicio.SelectedDate = null;
            FechaTermino.SelectedDate = null;
            cmbTipoEvento.SelectedIndex = -1;
            cmbModalidadServicio.SelectedIndex = -1;
            txtAsistentes.Text = "";
            txtValorTotal.Text = "";
            txtPersonalAdicional.Text = "";
            chbRealizado.IsChecked = false;
            txtObservaciones.Text = "";
            TPHoraInicioEvento.SelectedTime = null;
            TPHoraTerminoEvento.SelectedTime = null;

        }

        private async void BuscarPorContrato()
        {
            TContrato tcontrato = new TContrato();
            Contrato contrato = tcontrato.GetEntity(txtBuscarPorContrato.Text);

            if (contrato != null)
            {
                txtRut.Text = contrato.Cliente.RutCliente;
                txtRazonSocial.Text = contrato.Cliente.RazonSocial;
                txtNombre.Text = contrato.Cliente.NombreContacto;
                txtEmail.Text = contrato.Cliente.MailContacto;
                txtDireccion.Text = contrato.Cliente.Direccion;
                txtTelefono.Text = contrato.Cliente.Telefono;
                cmbActividadEmpresa.SelectedValue = contrato.Cliente.IdActividadEmpresa;
                cmbTipoEmpresa.SelectedValue = contrato.Cliente.IdTipoEmpresa;
            }

            else
            {
                await this.ShowMessageAsync("Administrador de contratos", "Usuario no encontrado");
            }

        }

        private async void BuscarPorRut()
        {
            TCliente tcliente = new TCliente();
            Cliente cliente = tcliente.GetEntity(txtBuscarPorRut.Text);
            if (cliente != null)
            {
                txtRut.Text = cliente.RutCliente;
                txtRazonSocial.Text = cliente.RazonSocial;
                txtNombre.Text = cliente.NombreContacto;
                txtEmail.Text = cliente.MailContacto;
                txtDireccion.Text = cliente.Direccion;
                txtTelefono.Text = cliente.Telefono;
                cmbActividadEmpresa.SelectedValue = cliente.IdActividadEmpresa;
                cmbTipoEmpresa.SelectedValue = cliente.IdTipoEmpresa;
            }

            else
            {
                await this.ShowMessageAsync("Administrador de contratos", "Usuario no encontrado");
            }
        }

        private void CargarTipoEmpresa()
        {
            TActividadEmpresa ae = new TActividadEmpresa();
            cmbActividadEmpresa.ItemsSource = ae.GetEntities();
            cmbActividadEmpresa.SelectedValuePath = "IdActividadEmpresa";
            cmbActividadEmpresa.DisplayMemberPath = "Descripcion";
        }

        private void CargarActividadEmpresa()
        {
            TTipoEmpresa te = new TTipoEmpresa();
            cmbTipoEmpresa.ItemsSource = te.GetEntities();
            cmbTipoEmpresa.SelectedValuePath = "IdTipoEmpresa";
            cmbTipoEmpresa.DisplayMemberPath = "Descripcion";
        }

        private void CargarModalidadSerivicio()
        {
            TModalidadServicio tmodalidadservicio = new TModalidadServicio();
            var filtromodalidad = from l in tmodalidadservicio.GetEntities()
                                  where l.IdTipoEvento == Convert.ToInt32(cmbTipoEvento.SelectedValue)

                                  select new
                                  {
                                      IdModalidadServicio = l.IdModalidad,
                                      Descripcion = l.Nombre
                                  };

            cmbModalidadServicio.ItemsSource = filtromodalidad;
            cmbModalidadServicio.SelectedValuePath = "IdModalidadServicio";
            cmbModalidadServicio.DisplayMemberPath = "Descripcion";
        }

        private void CargarTipoEvento()
        {
            TTipoEvento ttipoevento = new TTipoEvento();
            List<TipoEvento> tipoevento = ttipoevento.GetEntities();
            cmbTipoEvento.ItemsSource = tipoevento;
            cmbTipoEvento.SelectedValuePath = "IdTipoEvento";
            cmbTipoEvento.DisplayMemberPath = "Descripcion";
        }

        private void LimpiarUsuario()
        {
            //Limpia campos de cliente
            txtRut.Text = "";
            txtRazonSocial.Text = "";
            txtNombre.Text = "";
            txtEmail.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            cmbActividadEmpresa.SelectedIndex = -1;
            cmbTipoEmpresa.SelectedIndex = -1;
        }

        private void SoloAceptoNumeros(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SoloAceptoLetras(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[A-Za-z]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CerrarVentana()
        {
            this.Close();
        }

        // Metodo Calculo Valor Total

        private void CalculoValor()
        {
            ValorTotal = ValorBase + RecargoAsistente + RecargoPersonal;
            txtValorTotal.Text = ValorTotal.ToString();
        }

    }
}