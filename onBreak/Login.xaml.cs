﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

namespace onBreak
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void pbuser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BTNlogin_Click(sender, e);
            }
        }

        private async void BTNlogin_Click(object sender, RoutedEventArgs e)
        {
            if(txtuser.Text == "admin" && pbuser.Password == "123")
            {
                Selector sec = new Selector();
                this.Close();
                sec.ShowDialog();
            }
            else
            {
                await this.ShowMessageAsync("Error", "usuario y/o contraseña no corresponden");
            }
        }
    }
}
