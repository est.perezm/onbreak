﻿#pragma checksum "..\..\Selector.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "EC83CE63FC7369497FAF533445296468B84A6267A2D7C2AECD2F00412A0CD518"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MahApps.Metro.Behaviours;
using MahApps.Metro.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using onBreak;


namespace onBreak {
    
    
    /// <summary>
    /// Selector
    /// </summary>
    public partial class Selector : MahApps.Metro.Controls.MetroWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\Selector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlClientes;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Selector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlListaClientes;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\Selector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image tlListaCtes;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\Selector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlContratos;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\Selector.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlListaContratos;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/onBreak;component/selector.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Selector.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tlClientes = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 17 "..\..\Selector.xaml"
            this.tlClientes.Click += new System.Windows.RoutedEventHandler(this.TlClientes_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tlListaClientes = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 22 "..\..\Selector.xaml"
            this.tlListaClientes.Click += new System.Windows.RoutedEventHandler(this.tlListaClientes_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.tlListaCtes = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.tlContratos = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 28 "..\..\Selector.xaml"
            this.tlContratos.Click += new System.Windows.RoutedEventHandler(this.tlContratos_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.tlListaContratos = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 34 "..\..\Selector.xaml"
            this.tlListaContratos.Click += new System.Windows.RoutedEventHandler(this.TlListaContratos_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

