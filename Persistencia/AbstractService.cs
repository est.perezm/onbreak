﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public abstract class AbstractService <E>
    {
        protected OnBreakEntities bd = new OnBreakEntities();

        public abstract void AddEntity(E entity);
        public abstract void UpdateEntity(E entity);
        public abstract void DeleteEntity(object key);
        public abstract E GetEntity(object key);
        public abstract List<E> GetEntities();

    }
}
