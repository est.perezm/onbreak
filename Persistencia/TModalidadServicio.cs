﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class TModalidadServicio : AbstractService<ModalidadServicio>
    {
        public override void AddEntity(ModalidadServicio entity)
        {
            ModalidadServicio modalidad = GetEntity(entity.IdModalidad);
            if (modalidad == null)
            {
                bd.ModalidadServicio.Add(entity);
                bd.SaveChanges();
            }
            else
            {
                throw new ArgumentException("Ya existe registro");
            }
        }

        public override void DeleteEntity(object key)
        {
            ModalidadServicio modalidad = GetEntity(key);
            if (modalidad != null)
            {
                bd.ModalidadServicio.Remove(modalidad);
            }
            else
            {
                throw new NotImplementedException("No es posible eliminar");
            }
        }

        public override List<ModalidadServicio> GetEntities()
        {
            return bd.ModalidadServicio.ToList<ModalidadServicio>();
        }

        public override ModalidadServicio GetEntity(object Key)
        {
            return bd.ModalidadServicio.Where(a => a.IdModalidad ==(string) Key).FirstOrDefault<ModalidadServicio>();
        }

        public override void UpdateEntity(ModalidadServicio entity)
        {
            ModalidadServicio modalidad = GetEntity(entity.IdModalidad);
            if (modalidad == null)
            {
                throw new NotImplementedException("No existe registro para actualizar");
            }
            else
            {
                modalidad.IdModalidad = entity.IdModalidad;
                modalidad.Nombre = entity.Nombre;
                modalidad.ValorBase = entity.ValorBase;
                modalidad.PersonalBase = entity.PersonalBase;
                modalidad.IdTipoEvento = entity.IdTipoEvento;
                bd.SaveChanges();
            }
        }
    }
}
