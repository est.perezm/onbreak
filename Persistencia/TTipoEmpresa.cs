﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class TTipoEmpresa : AbstractService<TipoEmpresa>
    {
        public override void AddEntity(TipoEmpresa entity)
        {
            TipoEmpresa empresa = GetEntity(entity.IdTipoEmpresa);
            if (empresa == null)
            {
                bd.TipoEmpresa.Add(entity);
                bd.SaveChanges();
            }
            else
            {
                throw new ArgumentException("Ya existe registro");
            }
        }

        public override void DeleteEntity(object key)
        {
            TipoEmpresa empresa = GetEntity(key);
            if (empresa != null)
            {
                bd.TipoEmpresa.Remove(empresa);
            }
            else
            {
                throw new NotImplementedException("No es posible eliminar");
            }
        }

        public override List<TipoEmpresa> GetEntities()
        {
            return bd.TipoEmpresa.ToList<TipoEmpresa>();
        }

        public override TipoEmpresa GetEntity(object Key)
        {
            return bd.TipoEmpresa.Where(a => a.IdTipoEmpresa == (int)Key).FirstOrDefault<TipoEmpresa>();
        }

        public override void UpdateEntity(TipoEmpresa entity)
        {
            TipoEmpresa empresa = GetEntity(entity.IdTipoEmpresa);
            if (empresa == null)
            {
                throw new NotImplementedException("No existe registro para actualizar");
            }
            else
            {
                empresa.IdTipoEmpresa = entity.IdTipoEmpresa;
                empresa.Descripcion = entity.Descripcion;
                bd.SaveChanges();
            }
        }
    }
}
