﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    
        public class TTipoEvento : AbstractService<TipoEvento>
        {
            public override void AddEntity(TipoEvento entity)
            {
                TipoEvento evento = GetEntity(entity.IdTipoEvento);
                if (evento == null)
                {
                    bd.TipoEvento.Add(entity);
                    bd.SaveChanges();
                }
                else
                {
                    throw new ArgumentException("Ya existe registro");
                }
            }

            public override void DeleteEntity(object key)
            {
                TipoEvento evento = GetEntity(key);
                if (evento != null)
                {
                    bd.TipoEvento.Remove(evento);
                }
                else
                {
                    throw new NotImplementedException("No es posible eliminar");
                }
            }

            public override List<TipoEvento> GetEntities()
            {
                return bd.TipoEvento.ToList<TipoEvento>();
            }

            public override TipoEvento GetEntity(object Key)
            {
                return bd.TipoEvento.Where(a => a.IdTipoEvento == (int)Key).FirstOrDefault<TipoEvento>();
            }

        public TipoEvento GetDescripcion(object Key)
        {
            return bd.TipoEvento.Where(a => a.Descripcion == (string)Key).FirstOrDefault<TipoEvento>();
        }

        public override void UpdateEntity(TipoEvento entity)
            {
                TipoEvento evento = GetEntity(entity.IdTipoEvento);
                if (evento == null)
                {
                    throw new NotImplementedException("No existe registro para actualizar");
                }
                else
                {
                    evento.IdTipoEvento = entity.IdTipoEvento;
                    evento.Descripcion = entity.Descripcion;
                    bd.SaveChanges();
                }
            }
        }
}
