﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class TContrato : AbstractService<Contrato>
    {
        public override void AddEntity(Contrato entity)
        {
            Contrato contrato = GetEntity(entity.Numero);
            if (contrato == null)
            {
                bd.Contrato.Add(entity);
                bd.SaveChanges();

            }
            else
            {
                throw new ArgumentException("Ya existe registro");

            }
        }

            public override void DeleteEntity(object key)
            {
            Contrato contrato = GetEntity(key);
            if (contrato != null)
            {
                bd.Contrato.Remove(contrato);
            }
            else
            {
                throw new NotImplementedException("No es posible eliminar");
            }
            }
        public override List<Contrato> GetEntities()
        {
            return bd.Contrato.ToList<Contrato>();

        }
        public override Contrato GetEntity(object Key)
        {
            return bd.Contrato.Where(a => a.Numero == (string)Key).FirstOrDefault<Contrato>();
        }

        public override void UpdateEntity(Contrato entity)
        {
            Contrato contrato = GetEntity(entity.Numero);
            if (contrato == null)
            {
                throw new NotImplementedException("No existe registro para actualizar");
            }
            else
            {
                contrato.Numero = entity.Numero;
                contrato.Creacion = entity.Creacion;
                contrato.Termino = entity.Termino;
                contrato.RutCliente = entity.RutCliente;
                contrato.IdModalidad = entity.IdModalidad;
                contrato.IdTipoEvento = entity.IdTipoEvento;
                contrato.FechaHoraInicio = entity.FechaHoraInicio;
                contrato.FechaHoraTermino = entity.FechaHoraTermino;
                contrato.Asistentes = entity.Asistentes;
                contrato.PersonalAdicional = entity.PersonalAdicional;
                contrato.Realizado = entity.Realizado;
                contrato.ValorTotalContrato = entity.ValorTotalContrato;
                contrato.Observaciones = entity.Observaciones;
                bd.SaveChanges();
            }
        }

        public void UpdateEstadoContrato(Contrato entity)
        {
            Contrato contrato = GetEntity(entity.Numero);
            if (contrato == null)
            {
                throw new NotImplementedException("No existe registro para actualizar");
            }
            else
            {
                contrato.Realizado = entity.Realizado;
                contrato.Termino = entity.Termino;
                bd.SaveChanges();
            }
        }
    }
}
