﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class TCliente : AbstractService<Cliente>
    {
        public override void AddEntity(Cliente entity)
        {
            AgregarCliente(entity);
        }

        public override void DeleteEntity(object key)
        {
            EliminarCliente(key);
        }

        public override void UpdateEntity(Cliente entity)
        {
            ModificarCliente(entity);
        }

        public override List<Cliente> GetEntities()
        {
            return bd.Cliente.ToList();
        }

        public override Cliente GetEntity(object key)
        {
            return bd.Cliente.Where(a => a.RutCliente == (string)key).FirstOrDefault();
        }

      

       

        private void AgregarCliente(Cliente entity)
        {
            Cliente cliente = GetEntity(entity.RutCliente);
            if (cliente == null)
            {
                bd.Cliente.Add(entity);
                bd.SaveChanges();

            }
            else
            {
                throw new ArgumentException("No es posible registrar el cliente" + "debido a que ya existe en los registros");
            }
        }

        private void EliminarCliente(object key)
        {
            Cliente cliente = GetEntity(key);
            if (cliente != null)
            {
                bd.Cliente.Remove(cliente);
                bd.SaveChanges();
            }

            else
            {
                throw new ArgumentException("El cliente no se encuentra registrado en la base de datos");
            }
        }

        private void ModificarCliente(Cliente entity)
        {
            Cliente cliente = GetEntity(entity.RutCliente);
            if (cliente == null)
            {
                throw new ArgumentException("No se puede actualizar el registro, ya que no existe el cliente");

            }
            else
            {
                cliente.RutCliente = entity.RutCliente;
                cliente.RazonSocial = entity.RazonSocial;
                cliente.NombreContacto = entity.NombreContacto;
                cliente.MailContacto = entity.MailContacto;
                cliente.Direccion = entity.Direccion;
                cliente.Telefono = entity.Telefono;
                cliente.IdActividadEmpresa = entity.IdActividadEmpresa;
                cliente.IdTipoEmpresa = entity.IdTipoEmpresa;
                bd.SaveChanges();
            }
        }
    }
}
