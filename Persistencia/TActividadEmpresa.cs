﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class TActividadEmpresa : AbstractService<ActividadEmpresa>
    {
        public override void AddEntity(ActividadEmpresa entity)
        {
            ActividadEmpresa actividad = GetEntity(entity.IdActividadEmpresa);
            if (actividad == null)
            {
                bd.ActividadEmpresa.Add(entity);
                bd.SaveChanges();
            }
            else
            {
                throw new ArgumentException("Ya existe registro");
            }
        }

        public override void DeleteEntity(object key)
        {
            ActividadEmpresa actividad = GetEntity(key);
            if (actividad != null)
            {
                bd.ActividadEmpresa.Remove(actividad);
            }
            else
            {
                throw new NotImplementedException("No es posible eliminar");
            }
        }

        public override List<ActividadEmpresa> GetEntities()
        {
            return bd.ActividadEmpresa.ToList<ActividadEmpresa>();
        }

        public override ActividadEmpresa GetEntity(object Key)
        {
            return bd.ActividadEmpresa.Where(a => a.IdActividadEmpresa == (int)Key).FirstOrDefault<ActividadEmpresa>();
        }

        public override void UpdateEntity(ActividadEmpresa entity)
        {
            ActividadEmpresa actividad = GetEntity(entity.IdActividadEmpresa);
            if (actividad == null)
            {
                throw new NotImplementedException("No existe registro para actualizar");
            }
            else
            {
                actividad.IdActividadEmpresa = entity.IdActividadEmpresa;
                actividad.Descripcion = entity.Descripcion;
                bd.SaveChanges();
            }
        }
    }
}
